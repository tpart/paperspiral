-- Size of AN Paper (in mm)
a n = both (1000*) $ (x, x * sqrt 2)
    where x = sqrt $ (1 / 2 ** n) / sqrt 2

both f (a,b) = (f a, f b)