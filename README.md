# Paper
This repository contains code related to A series paper.

Paper in this series has an aspect ratio of $\sqrt{2}$. Additionally, one piece of A0 paper is exactly one square meter in area. Paper sizes can be calculated using `src/PaperSize.hs`.

Additionally, a metric paper spiral can be generated as an svg using `stack run -- -o paper.svg -w 500`.

![Spiral](./paper.svg)