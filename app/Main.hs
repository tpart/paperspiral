{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine

iterations :: Int
iterations = 17

paperH :: Diagram B
paperH = square 1 # scaleX (sqrt 2) # lc black

paperV :: Diagram B
paperV = paperH # rotateBy (1/4)

paperList :: Int -> Diagram B
paperList 0 = paperH === paperH
paperList n | n     `mod` 4 == 0 = paperH === paperList (n-1) # translate (r2 (-0.5, 0)) # shrinkPaper
paperList n | (n+1) `mod` 4 == 0 = paperList (n-1) # translate (r2 (0, 0.5)) # shrinkPaper ||| paperV
paperList n | (n+2) `mod` 4 == 0 = paperList (n-1) # translate (r2 (-0.5, 0)) # shrinkPaper === paperH
paperList n | (n+3) `mod` 4 == 0 = paperV ||| paperList (n-1) # translate (r2 (0, 0.5)) # shrinkPaper

shrinkPaper :: Diagram B -> Diagram B
shrinkPaper = scale (1 / sqrt 2)

main :: IO ()
main = mainWith $ paperList iterations